# vue-job-search

VUE project imitating the "google careers" website.

It uses:

- Vue
- Vuex
- Vue Router
- Fully made on typescript
- Axios
- Jest for testing
- Tailwind CSS for styling
- FontAwesome
